package classes;

/**
 * This class defines the user information
 */
public class User extends Person {

    //appropriate constructors. You can overload as needed
    public User(String name, String ID) {
        super(name, ID);
    }

    //appropriate constructors. You can overload as needed
    public User() {

    }
}
