package classes;

//This class is to define person attributes
public class Person {

    private String name; // STORES THE NAME OF THE PERSON 
    private String ID; // STORES THE ID OF THE PERSON 


    //appropriate constructors. You can overload as needed
    public Person(String name, String ID) {
        setName(name);
        setID(ID);
    }

    //appropriate constructors. You can overload as needed
    public Person() {
    }

    public String getName() { // RETURNS THE NAME OF THE PERSON
        return name;
    }

    public void setName(String name) { // SETTING THE NAME OF THE PERSON
        this.name = name;
    }

    public String getID() { // RETURNS THE ID OF THE PERSON
        return ID;
    }

    public void setID(String ID) { // SETTING THE ID OF THE PERSON
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "Person{"
                + "name='" + name + '\''
                + ", ID=" + ID
                + '}';
    }
}
