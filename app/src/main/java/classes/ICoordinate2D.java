package classes;

// TO EASE IMPLEMENTING THE COORDINATE WITH SPECIFIC WAY
public interface ICoordinate2D {

    /**
     * It is X
     */
    double getLat();// GETTING LATITUDE

    /**
     * It is X
     */
    void setLat(double x);// SETTING LATITUDE

    /**
     * It is Y
     */
    double getLon();// GETTING LONGITITUDE

    /**
     * It is Y
     */
    void setLon(double y);// SETTING LONGITITUDE

}
