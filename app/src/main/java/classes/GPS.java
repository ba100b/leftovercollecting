package classes;


public class GPS implements ICoordinate2D {

    private static final double ALT = 1.0;// Stores the ALTITIUDE
    private double lat; //x Stores the LATITUDE
    private double lon; //Y Stores the LONGITITUDE

    //appropriate constructors. You can overload as needed
    public GPS() {
    }

    /**
     * 3D Coordinate
     */
    public GPS(double lat, double lon, double altitude) {
        setLat(lat);
        setLon(lon);
    }

    /**
     * 2D Coordinate
     */
    public GPS(double lat, double lon) {
        this(lat, lon, ALT);
    }


    @Override
    public double getLat() {// RETURNS THE LATITUDE
        return lat;
    }

    @Override
    public void setLat(double latitude) { // SETTING LATITUDE
        if ((latitude > -85) && (latitude < 85))
            this.lat = latitude;
        else
            throw new IllegalArgumentException("Wrong latitude");
    }

    @Override
    public double getLon() { // RETURNS LONGITITUDE
        return lon;
    }

    @Override
    public void setLon(double longitude) {// SETTING LONGITITUDE
        if ((longitude > -180) && (longitude < 180))
            this.lon = longitude;
        else
            throw new IllegalArgumentException("Wrong longitude");
    }

    @Override
    public String toString() { // HOW TO PRINT GPS.
        return "GPS{" + "lat=" + lat + ", lon=" + lon + '}';
    }


}
