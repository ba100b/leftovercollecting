package classes;

//This class is to define charity attributes
public class Charity {

    private String name;// Stores the name of the charity
    private String ID;// Stores the ID of the charity 

    //appropriate constructors. You can overload as needed
    public Charity(String name, String ID) {
        setName(name);
        setID(ID);
    }

    //appropriate constructors. You can overload as needed
    public Charity() {

    }

    //RETURNS the name of the charity
    public String getName() {
        return name;
    }

    //Setting the name of charity
    public void setName(String name) {
        this.name = name;
    }

    //GETTING CHARITY ID
    public String getID() {
        return ID;
    }

    //SETTING CHARITY ID
    public void setID(String ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {//THE WAY TO PRINT CHARITY INFORMATIONS
        return "Charity{"
                + "name='" + name + '\''
                + ", ID='" + ID + '\''
                + '}';
    }
}
