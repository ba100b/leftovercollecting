package classes;

public class Order {

    /**
     * The location of the Aazemah
     */
    private GPS location;

    private float FoodAmountInKG; // THE AMOUNT OF FOOD

    /**
     * What kind of foods there are
     */
    private String typeOfFoods;

    /**
     * date and time of the Aazemah
     */
    private String date;

    /**
     * The one who did the Aazemah
     */
    private User aazem;

    /**
     * The charity that is responsible of taking the food and then delivering it
     * to poor people
     */
    private Charity responsibleCharity;

    /**
     * Current status of the order
     */
    private OrderStatus status;

    private String locationDescription;

    private String orderID;

    //appropriate constructors. You can overload as needed
    public Order(GPS location, String locationDescription, float foodAmountInKG, String typeOfFoods, String date,
                 User aazem, Charity responsibleCharity, String orderID) {
        setLocation(location);

        setFoodAmountInKG(foodAmountInKG);
        setLocationDescription(locationDescription);
        setTypeOfFood(typeOfFoods);

        setDate(date);
        setAazem(aazem);
        setResponsibleCharity(responsibleCharity);
        setStatus(OrderStatus.PENDING);
        setOrderID(orderID);
    }

    //appropriate constructors. You can overload as needed
    public Order() {
        location = new GPS();
        aazem = new User();
        status = OrderStatus.PENDING;
        responsibleCharity = new Charity();
    }

    public String getOrderID() {// RETURNS THE ORDER ID
        return orderID;
    }

    public void setOrderID(String orderID) {// SETTING ORDER'S ID
        this.orderID = orderID;
    }

    public GPS getLocation() { // RETURNS THE LOCATION OF AAZEMAH.
        return location;
    }

    public void setLocation(GPS location) {// SETTING THE LOCATION OF AAZEMAH.
        this.location = location;
    }

    public String getLocationDescription() { // RETURNS THE DESCRIPTION LOCATION OF AAZEMAH 
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) { // SETTING THE DESCRIPTION LOCATION OF AAZEMAH
        this.locationDescription = locationDescription;
    }

    public float getFoodAmountInKG() { // RETURNS THE AMOUNT OF FOOD
        return FoodAmountInKG;
    }

    public void setFoodAmountInKG(float foodAmountInKG) { // SEETING THE AMOUNT OF FOOD 
        FoodAmountInKG = foodAmountInKG;
    }

    public String getTypeOfFoods() { // RETURNS THE FOOD TYPE THAT GIVEN TO CHARITY 
        return typeOfFoods;
    }

    public void setTypeOfFoods(String typeOfFoods) { // SETTING THE TYPE OF FOOD TO GIVE CHARITY 
        this.typeOfFoods = typeOfFoods;
    }

    public void setTypeOfFood(String typeOfFoods) { // SETTING THE TYPE OF FOOD TO GIVE CHARITY
        this.typeOfFoods = typeOfFoods;
    }

    public String getDate() { // RETURNS THE DATE OF AAZEMAH .
        return date;
    }

    public void setDate(String date) { // SETTING THE DATE OF AAZEMAH.
        this.date = date;
    }

    public User getAazem() { // RETURNS TEH USER WHO WILL USE THE APP.
        return aazem;
    }

    public void setAazem(User aazem) { // SETTING THE USER TO REQUSET.
        this.aazem = aazem;
    }

    public Charity getResponsibleCharity() { // RETURNS THE CHARITY WHOS RESPONSIBLE OF THE AAZEMAH.
        return responsibleCharity;
    }

    public void setResponsibleCharity(Charity responsibleCharity) { // SETTING THE CHARITY WHOSE RESPONSIBLE OF THE AAZEMAH.
        this.responsibleCharity = responsibleCharity;
    }

    public OrderStatus getStatus() { // RETURNS THE STATUS OF THE ORDER.
        return status;
    }

    public void setStatus(OrderStatus status) { // SETTING THE STATUS OF THE ORDER
        this.status = status;
    }

    @Override
    public String toString() {
        return "Order{"
                + "location=" + location
                + ", FoodAmountInKG=" + FoodAmountInKG
                + ", typeOfFoods='" + typeOfFoods + '\''
                + ", date='" + date + '\''
                + ", aazem=" + aazem
                + ", responsibleCharity=" + responsibleCharity
                + ", status=" + status
                + ", locationDescription='" + locationDescription + '\''
                + '}';
    }

    public enum OrderStatus {

        PENDING(), ACCEPTED(), REJECTED();

        @Override
        public String toString() {
            return name();
        }
    }
}
