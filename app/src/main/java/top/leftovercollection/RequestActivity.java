package top.leftovercollection;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import classes.Order;

/**
 This class defines the way we request order from the charities
 It takes the order information from the user and send
 it to the database
 */
public class RequestActivity extends AppCompatActivity {

    Order order;

    LocationManager locationManager;
    TextView welcome;

    /**
     * to check if the app was closed and now opened and order is just requested
     */
    boolean isStartedOrRequest;


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    //get reference of the root in the database
    DatabaseReference rootRef = database.getReference("Order");

    //get reference to the user information
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();





    /**
     * this variable indicate whether the location has stored or not
     */
    boolean isLocationAvailable = false;
    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location deviceGPS) {
            Log.d("mylocation", deviceGPS.getLongitude() + ", " + deviceGPS.getLatitude());

            //get our GPS instance from order instance and set its attributes
            //to match deviceGPS longitude and latitude
            order.getLocation().setLon(deviceGPS.getLongitude());
            order.getLocation().setLat(deviceGPS.getLatitude());


            String cityName;
            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(deviceGPS.getLatitude(),
                        deviceGPS.getLongitude(), 1);
                if (addresses.size() > 0) {
                    Log.d("address:", addresses.get(0).getLocality());
                    cityName = addresses.get(0).getLocality();


                    //set an arbitrary ID to charity
                    String tempID = "31242";
                    order.getResponsibleCharity().setID(tempID);
                    order.getResponsibleCharity().setName(cityName);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            isLocationAvailable = true;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        new MyToolBar().setupToolBar(this, R.id.requestToolbar);
        order = new Order();

        welcome = (TextView) findViewById(R.id.tv_welcome);
        welcome.setText("Welcome " + firebaseUser.getDisplayName());


    }

    @Override
    protected void onResume() {
        //to check after coming back from another activity
        checkDataStatus();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_signOut:
                signOut();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void signOut() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        // firebaseUser is now signed out
                        startActivity(new Intent(RequestActivity.this, AuthenticationActivity.class));
                        finish();
                    }
                });
    }

    public void setGPSInfo() {

        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the firebaseUser grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locationListener);

        Runnable run1 = new Runnable() {
            @Override
            public void run() {
                while (true)
                    if (isLocationAvailable) {
                        locationManager.removeUpdates(locationListener);
                        break;
                    }
            }
        };

        Thread thread1 = new Thread(run1);
        thread1.start();
    }




    public void requestCar(View view) {

        Toast.makeText(RequestActivity.this, "Please wait...", Toast.LENGTH_SHORT).show();
        setGPSInfo();


        setOrderInfo();

        setUserInfo();

        sendOrderToDB();


        isStartedOrRequest = true;

        TextView welcome = (TextView) findViewById(R.id.tv_welcome);
        //welcome.setText(order.toString());
        welcome.setText(firebaseUser.getDisplayName() + ", " + getResources().getString(R.string.requestActivity_orderIsSent));
    }






    public void setUserInfo() {
        if (firebaseUser != null) {
            String name = firebaseUser.getDisplayName();
            String userID = firebaseUser.getUid();

            order.getAazem().setName(name);
            order.getAazem().setID(userID);
        }
    }

    /**
     * get order info from TextView and set their values to order attributes
     */
    public void setOrderInfo() {
        EditText et_locationDescription = (EditText) findViewById(R.id.et_locationDescription);
        EditText et_foodType = (EditText) findViewById(R.id.et_footType);
        EditText et_quantity = (EditText) findViewById(R.id.et_quantity);

        order.setLocationDescription(et_locationDescription.getText().toString());
        order.setTypeOfFood(et_foodType.getText().toString());
        order.setFoodAmountInKG(Float.valueOf(et_quantity.getText().toString()));
        order.setStatus(Order.OrderStatus.PENDING);

        clearEditTexts(et_locationDescription, et_foodType, et_quantity);

        //order id
        int max = 88888;
        int min = 10000;
        order.setOrderID(String.valueOf(new Random().nextInt((max - min) + 1) + min));

        //get current time
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        order.setDate(currentDateTimeString);
    }

    public void sendOrderToDB() {
        if (!(order.getLocation().getLat() == 0 && order.getLocation().getLon() == 0)) {
            rootRef.setValue(order);
        }
    }


    public void openCharity(View view) {
        startActivity(new Intent(this, CharityActivity.class));
    }


    public void checkDataStatus() {
        ValueEventListener orderListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                order = dataSnapshot.getValue(Order.class);
                if (isStartedOrRequest) {
                    switch (order.getStatus().name()) {
                        case "ACCEPTED":
                            Toast.makeText(RequestActivity.this, "Your request has been Accepted", Toast.LENGTH_LONG).show();
                            welcome.setText(firebaseUser.getDisplayName() + ", Your request has been Accepted");
                            isStartedOrRequest = false;
                            break;
                        case "REJECTED":
                            Toast.makeText(RequestActivity.this, "Your request has been Rejected", Toast.LENGTH_SHORT).show();
                            welcome.setText("Sorry " + firebaseUser.getDisplayName() + ", Your request has been Rejected");
                            isStartedOrRequest = false;
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("Error log", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        rootRef.addValueEventListener(orderListener);
    }


    public void clearEditTexts(EditText... editTexts) {
        for (EditText editText : editTexts) {
            editText.setText("");
        }
    }

}