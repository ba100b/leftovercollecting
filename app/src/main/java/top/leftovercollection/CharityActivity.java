package top.leftovercollection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import classes.Order;


/**
 * This class defines the way charities accepts or rejects order
 * of the user
 *
 * @author Bassel Saeed
 * @since 29/5/2017
 */
public class CharityActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference rootRef = database.getReference("Order");

    TextView tv_orderInfo;
    Button btn_accept;
    Button btn_reject;

    Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charity);

        new MyToolBar().setupToolBar(this, R.id.charityToolbar);


        tv_orderInfo = (TextView) findViewById(R.id.tv_orderInfo);
        btn_accept = (Button) findViewById(R.id.btn_acceptRequest);
        btn_reject = (Button) findViewById(R.id.btn_rejectRequest);

        readDate();

        if (order != null) {
            displayInfo(order);
        }

    }



    public void readDate() {
        ValueEventListener orderListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                order = dataSnapshot.getValue(Order.class);

                if (order.getStatus() == Order.OrderStatus.PENDING) {
                    displayInfo(order);
                    showButtons();
                } else {
                    tv_orderInfo.setText("Waiting for the next order..");
                    hideButtons();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("Error log", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        rootRef.addValueEventListener(orderListener);
    }


    public void displayInfo(Order order) {
        tv_orderInfo = (TextView) findViewById(R.id.tv_orderInfo);
        tv_orderInfo.setVisibility(View.VISIBLE);
        String name = "Name: " + order.getAazem().getName();
        String foodType = "Food Type: " + order.getTypeOfFoods();
        String amount = "Amount: " + order.getFoodAmountInKG();
        String location = "Location: " + order.getLocationDescription();

        String info = String.format("%s\n%s\n%s\n%s", name, foodType, amount, location);
        Log.e("haha", "haha");
        tv_orderInfo.setText(info);
    }


    public void acceptRequest(View view) {
        order.setStatus(Order.OrderStatus.ACCEPTED);
        rootRef.setValue(order);
        hideButtons();
    }

    public void rejectRequest(View view) {
        order.setStatus(Order.OrderStatus.REJECTED);
        rootRef.setValue(order);
        hideButtons();
    }

    public void hideButtons() {
        btn_accept.setVisibility(View.INVISIBLE);
        btn_reject.setVisibility(View.INVISIBLE);
        tv_orderInfo.setText("Waiting for the next order..");
    }

    public void showButtons() {
        btn_accept.setVisibility(View.VISIBLE);
        btn_reject.setVisibility(View.VISIBLE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_signOut:
                signOut();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void signOut() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        // firebaseUser is now signed out
                        startActivity(new Intent(CharityActivity.this, AuthenticationActivity.class));
                        finish();
                    }
                });
    }


}
