package top.leftovercollection;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


/**
 * This class is to define and create the Toolbar
 * The toolbar contains the app title and Sign out option
 * @author Bassel
 * @since 09/05/2017
 *
 */
public class MyToolBar {

    public Toolbar setupToolBar(AppCompatActivity activity, @android.support.annotation.IdRes int id) {
        Toolbar myToolbar = (Toolbar) activity.findViewById(id);
        activity.setSupportActionBar(myToolbar);
        return myToolbar;
    }

}
